/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较并交换，使得每一轮循环后最大（或最小）的元素被放到正确的位置  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制比较的轮数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每轮比较的次数  
            if (a[j] > a[j + 1]) { // 如果前一个元素大于后一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end









