/**  
 * 冒泡排序函数  
 * 这是一个简单的排序算法，通过重复遍历要排序的列表，比较每对相邻的项，如果它们的顺序错误就把它们交换过来。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制遍历次数  
        for (int j = 0; j < n - i - 1; j++) { // 内层循环控制每次遍历需要比较的次数  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} // end
