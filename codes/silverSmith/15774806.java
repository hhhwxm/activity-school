/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较，将较大的元素交换到数组的末尾，经过多轮比较后，整个数组变得有序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 每一轮排序，最后一个元素已经是最大的，所以不需要再比较  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                // 交换a[j]和a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
