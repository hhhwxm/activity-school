/**  
 * 冒泡排序函数  
 * 遍历数组，通过相邻元素比较和交换，将最大（或最小）的元素逐渐“浮”到数组的一端。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 标记变量，用于检测本次循环是否发生了交换，如果没有交换说明数组已经有序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换 a[j] 和 a[j+1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记本次循环发生了交换  
                swapped = true;  
            }  
        }  
        // 如果在某一轮循环中没有发生交换，说明数组已经有序，直接退出  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
